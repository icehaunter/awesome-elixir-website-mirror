defmodule ElixirMirror.Repo do
  use Ecto.Repo,
    otp_app: :awesome_elixir_mirror,
    adapter: Ecto.Adapters.Postgres
end
