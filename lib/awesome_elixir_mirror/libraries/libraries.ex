defmodule ElixirMirror.Libraries do
  @moduledoc """
  Context, responsible for library list storage, updates and manipulation
  """

  alias ElixirMirror.Libraries.Category
  alias ElixirMirror.Repo

  def create_category(name, slug) do
    %Category{}
    |> Category.changeset(%{
      name: name,
      slug: slug
    })
    |> Repo.insert()
  end



  def list_all_categories() do
    Category |> Repo.all()
  end


  alias ElixirMirror.Libraries.Library

  def create_library(params) do
    %Library{}
    |> Library.changeset(params)
    |> Repo.insert()
  end



  def list_all_libraries() do
    Library |> Repo.all()
  end
end
