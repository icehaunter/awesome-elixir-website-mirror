defmodule ElixirMirror.Libraries.Category do
  use Ecto.Schema
  import Ecto.Changeset

  schema "categories" do
    field :name, :string
    field :slug, :string
  end

  @doc false
  def changeset(category, attrs \\ %{}) do
    category
    |> cast(attrs, ~w|name slug|a)
    |> validate_required(~w|name slug|a)
    |> validate_length(:name, max: 512)
    |> validate_length(:slug, max: 512)
    |> unique_constraint(:slug)
  end
end
