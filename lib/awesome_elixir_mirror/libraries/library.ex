defmodule ElixirMirror.Libraries.Library do
  use Ecto.Schema
  import Ecto.Changeset

  schema "libraries" do
    field :name, :string
    field :link, :string
    field :description, :string
    field :stars_count, :integer
    field :latest_commit, :naive_datetime

    belongs_to :category, ElixirMirror.Libraries.Category
  end

  @allowed_fields ~w|name link description stars_count latest_commit category_id|a

  def changeset(library, attrs \\ %{}) do
    library
    |> cast(attrs, @allowed_fields)
    |> validate_required(@allowed_fields)
    |> validate_length(:name, max: 256)
    |> validate_length(:link, max: 512)
    |> validate_number(:stars_count, greater_than_or_equal_to: 0)
    |> unique_constraint(:name, name: :libraries_category_id_name_index)
    |> assoc_constraint(:category)
  end
end
