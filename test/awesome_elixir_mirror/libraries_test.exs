defmodule ElixirMirror.LibrariesTest do
  use ElixirMirror.DataCase

  alias ElixirMirror.Libraries

  describe "create_category/2" do
    test "should properly create a category" do
      assert [] == Libraries.list_all_categories()
      assert {:ok, %Libraries.Category{} = category} = Libraries.create_category("Test", "test")
      assert [^category] = Libraries.list_all_categories()
      assert category.name == "Test"
      assert category.slug == "test"
    end

    test "should fail to create a category if one with the same slug exists" do
      assert [] == Libraries.list_all_categories()
      assert {:ok, %Libraries.Category{} = category} = Libraries.create_category("Test", "test")
      assert {:error, changeset} = Libraries.create_category("Other test", "test")

      assert "has already been taken" in errors_on(changeset).slug
    end
  end

  describe "create_library/2" do
    setup do
      {:ok, category} = Libraries.create_category("name", "slug")
      {:ok, other_category} = Libraries.create_category("other", "other")

      %{category: category, other_category: other_category}
    end

    test "should properly create a library", %{ category: category } do
      assert [] == Libraries.list_all_libraries()
      assert {:ok, %Libraries.Library{} = library} = Libraries.create_library(%{
        name: "exactor",
        link: "https://github.com/sasa1977/exactor",
        description: "Helpers for simpler implementation of GenServer based processes",
        stars_count: 623,
        latest_commit: ~N|2018-08-30 10:27:35|,
        category_id: category.id
      })
      assert [^library] = Libraries.list_all_libraries()
    end

    test "should fail to create a library if one with the same name exists in same category", %{ category: category } do
      assert [] == Libraries.list_all_libraries()
      assert {:ok, %Libraries.Library{} = library} = Libraries.create_library(%{
        name: "exactor",
        link: "https://github.com/sasa1977/exactor",
        description: "Helpers for simpler implementation of GenServer based processes",
        stars_count: 623,
        latest_commit: ~N|2018-08-30 10:27:35|,
        category_id: category.id
      })
      assert {:error, changeset} = Libraries.create_library(%{
        name: "exactor",
        link: "https://github.com/sasa1977/exactor",
        description: "Helpers for simpler implementation of GenServer based processes",
        stars_count: 623,
        latest_commit: ~N|2018-08-30 10:27:35|,
        category_id: category.id
      })

      assert "has already been taken" in errors_on(changeset).name
    end

    test "should create a library with the same name, but in a different category", %{ category: category, other_category: other } do
      assert [] == Libraries.list_all_libraries()
      assert {:ok, %Libraries.Library{} = library} = Libraries.create_library(%{
        name: "exactor",
        link: "https://github.com/sasa1977/exactor",
        description: "Helpers for simpler implementation of GenServer based processes",
        stars_count: 623,
        latest_commit: ~N|2018-08-30 10:27:35|,
        category_id: category.id
      })
      assert {:ok, _} = Libraries.create_library(%{
        name: "exactor",
        link: "https://github.com/sasa1977/exactor",
        description: "Helpers for simpler implementation of GenServer based processes",
        stars_count: 623,
        latest_commit: ~N|2018-08-30 10:27:35|,
        category_id: other.id
      })
    end
  end
end
