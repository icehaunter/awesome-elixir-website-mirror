# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :awesome_elixir_mirror,
  namespace: ElixirMirror,
  ecto_repos: [ElixirMirror.Repo]

# Configures the endpoint
config :awesome_elixir_mirror, ElixirMirrorWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "5+nItwmiM903AiwftMQtLWt55bmkzif2Jksi4SIj8f/IlMswi3BVjjYzIP5G9DsF",
  render_errors: [view: ElixirMirrorWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ElixirMirror.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
