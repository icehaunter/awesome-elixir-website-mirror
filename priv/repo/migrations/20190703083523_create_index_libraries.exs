defmodule ElixirMirror.Repo.Migrations.CreateIndexLibraries do
  use Ecto.Migration

  def change do
    create unique_index("libraries", [:category_id, :name])
  end
end
