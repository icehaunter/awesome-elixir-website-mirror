defmodule ElixirMirror.Repo.Migrations.CreateTableCategories do
  use Ecto.Migration

  def change do
    create table("categories") do
      add :name, :string, size: 512
      add :slug, :string, size: 512
    end

    create unique_index("categories", [:slug])
  end
end
