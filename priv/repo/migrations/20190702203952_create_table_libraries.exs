defmodule ElixirMirror.Repo.Migrations.CreateTableProducts do
  use Ecto.Migration

  def change do
    create table("libraries") do
      add :name, :string, size: 256
      add :link, :string, size: 512
      add :category_id, references("categories")
      add :description, :text
      add :stars_count, :integer
      add :latest_commit, :naive_datetime
    end
  end
end
